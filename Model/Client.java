package Model;

import java.io.Serializable;

public class Client implements Serializable {
    private static final long serialVersionUID=1L;
    private int id;
    private String firstname;
    private String lastname;
    private String address;
    private String postalCode;
    private String phoneNumber;
    private String clientType;

/*
    public Client(){
        this.id=0;
        this.firstname="";
        this.lastname="";
        this.address="";
        this.postalCode="";
        this.phoneNumber="";
        this.clientType="";
    }
*/

    public Client(int id, String firstname, String lastname, String address, String postalCode, String phoneNumber, String clientType){
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.postalCode = postalCode;
        this.phoneNumber = phoneNumber;
        this.clientType = clientType;
    }

    public Client(String firstname, String lastname, String address, String postalCode, String phoneNumber, String clientType){
        //this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.postalCode = postalCode;
        this.phoneNumber = phoneNumber;
        this.clientType = clientType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String toString(){
        String client = this.id+" "+this.firstname+" "+ this.lastname+" "+this.address+" "+this.phoneNumber+" "+this.postalCode+" "+this.clientType;
        return client;
    }


}
