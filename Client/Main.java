package Client;

import Client.View.MyFrame;
import Client.Controller.ClientController;
import Client.HelperClasses.*;

public class Main {
    public static void main(String[] args){
        ClientSocket socket = new ClientSocket("localhost", 8099);
        ClientController controller = new ClientController(socket);
        MyFrame frame = new MyFrame("Client Management Screen",controller);
        frame.setSize(700,600);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

       // controller.getItemWrite().getSocketOut().println("searchClientByID");
       // System.out.println("Client in Main");
    }
}
