package Client.Controller;

import Model.Client;
import Client.HelperClasses.*;
import java.util.ArrayList;

public class ClientController {
    private WriteRecordObject objectWrite;
    private WriteRecordItem itemWrite;
    private ReadRecordObject objectRead;

    public ClientController(ClientSocket socket){
        objectWrite = new WriteRecordObject(socket);
        itemWrite = new WriteRecordItem(socket);
        objectRead = new ReadRecordObject(socket);
    }

    public WriteRecordObject getObjectWrite()
    {
        return objectWrite;
    }

    public WriteRecordItem getItemWrite()
    {
        return itemWrite;
    }

    public ReadRecordObject getObjectRead() {
        return objectRead;
    }


    public Client searchClientByID(int clientID){
        itemWrite.getSocketOut().println("searchClientByID");
        itemWrite.getSocketOut().println(clientID);
        return objectRead.openObjectInputStream();
    }
/*
    public void fillTable(){

    }
*/
    public ArrayList<Client> searchClientByLastName(String lastname)
    {
        itemWrite.getSocketOut().println("searchClientByLastName");
        itemWrite.getSocketOut().println(lastname);
        return objectRead.openObjectInputStreamArrayList();
    }

    public ArrayList<Client> searchClientByClientType(String clientType)
    {
        itemWrite.getSocketOut().println("searchClientByClientType");
        itemWrite.getSocketOut().println(clientType);
        return objectRead.openObjectInputStreamArrayList();
    }

    public void addClient(Client client)
    {
        itemWrite.getSocketOut().println("addClient");
        objectWrite.openObjectOutputStream(client);
    }

    public void updateClient(Client client)
    {
        itemWrite.getSocketOut().println("updateClient");
        objectWrite.openObjectOutputStream(client);
    }

    public void deleteClient(int clientID)
    {
        itemWrite.getSocketOut().println("deleteClient");
        itemWrite.getSocketOut().println(clientID);
    }
}
