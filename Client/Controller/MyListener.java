package Client.Controller;

import Client.View.MyFrame;
import Model.Client;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

public class MyListener implements ActionListener{
    private MyFrame frame;
    //private ClientCMS client;
    private ClientController controller;
    public MyListener(MyFrame f, ClientController controller){
        frame=f;
        this.controller = controller;
      //  client= new ClientCMS();
        //controller.fillTable();
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        String searchItem= frame.searchField.getText().toString();
        Client clientSearched = null;
        ArrayList<Client> clientArrayList = null;
        StringWriter buffer = new StringWriter();
        PrintWriter writer = new PrintWriter(buffer);
        if(frame.rb1.isSelected() == true){
            frame.searchField.setText("Enter the client ID");
            if(e.getSource() == frame.search){
                frame.searchField.setText("searching...");
                clientSearched= controller.searchClientByID(Integer.parseInt(searchItem));
                frame.textArea.setText(clientSearched.toString());
                frame.clientIDField.setText(Integer.toString(clientSearched.getId()));
                frame.fnameField.setText(clientSearched.getFirstname());
                frame.lnameField.setText(clientSearched.getLastname());
                frame.addressField.setText(clientSearched.getAddress());
                frame.phoneField.setText(clientSearched.getPhoneNumber());
                frame.postalCodeField.setText(clientSearched.getPostalCode());
                frame.comboClient.setSelectedItem(clientSearched.getClientType());

            }
        }else if(frame.rb2.isSelected() == true){
            frame.searchField.setText("Enter Last Name");
            if(e.getSource() == frame.search){
                frame.searchField.setText("searching...");
                clientArrayList= controller.searchClientByLastName(searchItem);
                frame.textArea.setText(" ");
                if(clientArrayList != null) {
                    int count = 0;
                    while (clientArrayList.size() > count) {
                        writer.println(clientArrayList.get(count).toString());
                        System.out.println(clientArrayList.get(count).toString());
                        count++;
                    }
                    frame.textArea.setText(buffer.toString());
                }else{
                    JOptionPane.showMessageDialog(null, "No clients with that last name");
                }
            }

        }else if(frame.rb3.isSelected() == true) {
            frame.searchField.setText("Enter the Client Type R or C");
            if (e.getSource() == frame.search) {
                frame.searchField.setText("searching...");
                clientArrayList = controller.searchClientByClientType(searchItem);
                frame.textArea.setText(" ");
                if (clientArrayList != null) {
                    int count = 0;
                    while (clientArrayList.size() > count) {
                        frame.textArea.append(clientArrayList.get(count).toString());
                        System.out.println(clientArrayList.get(count).toString());
                        count++;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No clients with that last type");
                }
            }
        }
        if(e.getSource() == frame.save){
        String clientId = frame.clientIDField.getText();
        int clientID;
        if(clientId.equals("Client ID")){
            clientID =0;
        }else{
            clientID = Integer.parseInt(clientId);
        }
        //JComboBox cb= (JComboBox)e.getSource();
        Client recordClient = new Client(frame.fnameField.getText(),frame.lnameField.getText(),frame.addressField.getText(),frame.postalCodeField.getText(),frame.phoneField.getText(),frame.comboClient.getSelectedItem().toString());
        //if(controller.searchClientByID(clientID) == null){
            controller.addClient(recordClient);
            JOptionPane.showMessageDialog(null,"Client was successfully added");
     //   }else{
    //        controller.updateClient(recordClient);
    //        JOptionPane.showMessageDialog(null,"Client was successfully updated");
      //  }
        }else if(e.getSource() == frame.delete){
            String clientId = frame.clientIDField.getText();
            if(controller.searchClientByID(Integer.parseInt(clientId)) != null){
                controller.deleteClient(Integer.parseInt(clientId));
            }else{
                JOptionPane.showMessageDialog(null,"Client cannot be deleted");
            }
        }else if(e.getSource() == frame.clearSearch){
            frame.textArea.setText(" ");
            frame.searchField.setText("Enter your search here");
        }else if(e.getSource() == frame.clear){
            frame.clientIDField.setText(" ");
            frame.fnameField.setText(" ");
            frame.lnameField.setText(" ");
            frame.addressField.setText(" ");
            frame.phoneField.setText(" ");
            frame.postalCodeField.setText(" ");
        }
    }
}
