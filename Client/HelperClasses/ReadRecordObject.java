package Client.HelperClasses;

import Model.Client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class ReadRecordObject {
    private ObjectInputStream objectIn = null;
    private Client record;
    private ArrayList<Client> clientArrayList;

    public ReadRecordObject(ClientSocket socket){
       // record = new Client();
        clientArrayList = new ArrayList<Client>();
        try{
            objectIn = new ObjectInputStream(socket.getSocketConnection().getInputStream());
        }catch (IOException ex){
            System.err.println(ex.getStackTrace());
        }

    }

    public Client openObjectInputStream(){
        try{
            this.record = (Client) objectIn.readObject();
        }catch(IOException ex){
            System.err.println(ex.getStackTrace());
        }catch(NoSuchElementException ex){
            System.err.println("Invalid Exception");
        }catch(ClassNotFoundException ex){
            System.err.println("Class not Found");
        }
        //System.out.println(record.getLastname().toString());
        return this.record;
    }

    public ArrayList<Client> openObjectInputStreamArrayList(){

        try{
            while(true) {
                record = (Client) objectIn.readObject();
                clientArrayList.add(record);
            }
        }catch(IOException ex){
            System.err.println("Error writing to a file");
        }catch(NoSuchElementException ex){
            System.err.println("Invalid Exception");
        }catch(ClassNotFoundException ex){
            System.err.println("Class not Found");
        }
        return clientArrayList;
    }
}
