package Client.HelperClasses;

import Model.Client;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.NoSuchElementException;


public class WriteRecordObject {
	private ObjectOutputStream objectOut = null;
	private Client record = null;

	public WriteRecordObject(ClientSocket socket){
		try{
		objectOut = new ObjectOutputStream(socket.getSocketConnection().getOutputStream());
		}catch (IOException ex){
			System.err.println(ex.getStackTrace());
		}
	}

	public void openObjectOutputStream(Client client){
		try{
			record = client;
			objectOut.writeObject(record);
		}catch(IOException ex){
			System.err.println(ex.getStackTrace());
		}catch(NoSuchElementException ex){
			System.err.println("Invalid Exception");
		}
	}
}
