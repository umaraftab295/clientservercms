package Client.HelperClasses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientSocket {

    private Socket socketConnection;

    public Socket getSocketConnection() {
        return socketConnection;
    }

    public void setSocketConnection(Socket socketConnection) {
        this.socketConnection = socketConnection;
    }


    public ClientSocket(String serverName, int portNumber) {
        try {
            socketConnection = new Socket(serverName, portNumber);
        } catch (IOException e) {
            System.err.println(e.getStackTrace());
        }
    }


}
