package Client.HelperClasses;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class WriteRecordItem {
    private BufferedReader stdIn;
    private BufferedReader socketIn;
    private PrintWriter socketOut;

    public BufferedReader getStdIn() {
        return stdIn;
    }

    public void setStdIn(BufferedReader stdIn) {
        this.stdIn = stdIn;
    }

    public BufferedReader getSocketIn() {
        return socketIn;
    }

    public void setSocketIn(BufferedReader socketIn) {
        this.socketIn = socketIn;
    }

    public PrintWriter getSocketOut() {
        return socketOut;
    }

    public void setSocketOut(PrintWriter socketOut) {
        this.socketOut = socketOut;
    }

    public WriteRecordItem(ClientSocket socket){
        try{
            stdIn = new BufferedReader(new InputStreamReader(System.in));
            socketIn = new BufferedReader(new InputStreamReader(socket.getSocketConnection().getInputStream()));
            socketOut = new PrintWriter((socket.getSocketConnection().getOutputStream()),true);
        }catch (IOException ex){
            System.err.println(ex.getStackTrace());
        }
    }

}
