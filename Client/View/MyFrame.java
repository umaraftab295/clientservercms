package Client.View;

import Client.Controller.ClientController;
import Client.Controller.MyListener;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import java.awt.*;

/*** Frame class with public fields, due to time constraints otherwise getters and setters were not a problem
 * since there was a confusion of deadline of Nov 5th instead of Nov 12th. ***/

public class MyFrame extends JFrame {
    public JLabel searchPar,searchRes,searchType,clientInfo,clientID,fname,lname,address,postalCode,phone,clientType;
    public JTextField searchField,clientIDField,fnameField,lnameField,addressField,postalCodeField,phoneField;
    public JButton save, delete,clear,search,clearSearch;
    public MyListener listener;
    public JTextArea textArea;
    public JPanel panelClient,panelSearch,panelSearchResults,panelFinalSearch;
    public GridBagConstraints cGrid,dGrid;
    public JScrollPane textScroll;
    public JRadioButton rb1,rb2,rb3;
    public ButtonGroup buttonGroup;
    public JTabbedPane tabbedPane;


    String[] typesOfClient = new String[] {"R","C"};
    // create a combo box with the fixed array:
    public JComboBox<String> comboClient;

    public MyFrame(String s, ClientController controller){
        super(s);
        listener = new MyListener(this,controller);

        /***Labels***/
        searchType = new JLabel("Select type of search to be performed :");
        searchPar = new JLabel("Enter the search parameter below:");
        searchRes = new JLabel("Search Results:");
        clientInfo= new JLabel("Client Information");
        clientID= new JLabel("Client Id");
        fname= new JLabel("First Name");
        lname= new JLabel("Last Name");
        address= new JLabel("Address");
        postalCode= new JLabel("Postal Code");
        phone= new JLabel("Phone");
        clientType= new JLabel("Client Type");

        /***RadioButtons***/
        rb1=new JRadioButton("Client ID");
        rb1.setBounds(100,50,100,30);
        rb2=new JRadioButton("Last Name");
        rb2.setBounds(100,100,100,30);
        rb3=new JRadioButton("Client Type");
        rb3.setBounds(100,150,100,30);

        buttonGroup=new ButtonGroup();
        buttonGroup.add(rb1);buttonGroup.add(rb2);buttonGroup.add(rb3);

        /***Buttons***/
        save = new JButton("Save");
        delete = new JButton("Delete");
        clear = new JButton("Clear");
        search = new JButton("Search");
        clearSearch = new JButton("ClearSearch");

        /***TextFields***/
        searchField = new JTextField("Enter your search item here");
        searchField.setSize (new Dimension(200,30));
        clientIDField = new JTextField("Client ID");
        clientIDField.setSize (new Dimension(200,30));
        clientIDField.setEditable(false);
        fnameField= new JTextField("First Name");
        fnameField.setSize (new Dimension(200,30));
        fnameField.setDocument(new JTextFieldLimit(20));
        lnameField= new JTextField("Last Name");
        lnameField.setSize (new Dimension(200,30));
        lnameField.setDocument(new JTextFieldLimit(20));
        addressField= new JTextField("Address");
        addressField.setSize (new Dimension(200,30));
        addressField.setDocument(new JTextFieldLimit(50));
        postalCodeField= new JTextField("Postal Code");
        postalCodeField.setSize (new Dimension(200,30));
        postalCodeField.setDocument(new JTextFieldLimit(7));
        phoneField= new JTextField("Phone");;
        phoneField.setSize (new Dimension(200,30));
        phoneField.setDocument(new JTextFieldLimit(12));

        /***Text Area and TextScroll***/
        textArea=new JTextArea(30, 35);
        textScroll = new JScrollPane(textArea);

        /***JPanels***/

        panelClient= new JPanel ( new GridBagLayout());
        panelSearch= new JPanel ( new GridBagLayout());
        panelSearchResults= new JPanel ( new GridBagLayout());
        panelFinalSearch = new JPanel(new FlowLayout());



        /*** ComboBoxes***/
        comboClient = new JComboBox<String>(typesOfClient);

        /**Grid*/
        cGrid= new GridBagConstraints();
        dGrid = new GridBagConstraints();

        /***Buttons added to action listener***/
        save.addActionListener(listener);
        delete.addActionListener(listener);
        clear.addActionListener(listener);
        search.addActionListener(listener);
        clearSearch.addActionListener(listener);
        rb1.addActionListener(listener);
        rb2.addActionListener(listener);
        rb3.addActionListener(listener);
        comboClient.addActionListener(listener);

        /***Text Area added to Panel***/
        cGrid.insets = new Insets(10,10,10,10);

        /***Arrange items on Panel***/
        cGrid.gridx=0;
        cGrid.gridy=0;
        panelSearch.add(searchType,cGrid);
        cGrid.gridx=0;
        cGrid.gridy=1;
        panelSearch.add(rb1,cGrid);
        cGrid.gridx=0;
        cGrid.gridy=2;
        panelSearch.add(rb2,cGrid);
        cGrid.gridx=0;
        cGrid.gridy=3;
        panelSearch.add(rb3,cGrid);
        cGrid.gridx=0;
        cGrid.gridy=5;
        panelSearch.add(searchPar,cGrid);
        cGrid.gridx=0;
        cGrid.gridy=6;
        panelSearch.add(searchField,cGrid);
        cGrid.gridx=1;
        cGrid.gridy=6;
        panelSearch.add(search,cGrid);
        cGrid.gridx=2;
        cGrid.gridy=6;
        panelSearch.add(clearSearch,cGrid);

        /*Search Results Panel*/
        cGrid.insets = new Insets(10,10,10,10);
        cGrid.gridx=0;
        cGrid.gridy=0;
        panelSearchResults.add(searchRes,cGrid);
        cGrid.gridx=1;
        cGrid.gridy=1;
        panelSearchResults.add(textScroll,cGrid);
        textScroll.setPreferredSize(new Dimension(200,200));


        panelFinalSearch.add(panelSearch);
        panelFinalSearch.add(panelSearchResults);

        /*** Client Panel Form ***/
        dGrid.insets = new Insets(10,10,10,10);
        dGrid.gridx=0;
        dGrid.gridy=0;

        panelClient.add(clientInfo,dGrid);
        dGrid.gridx=1;
        dGrid.gridy=1;
        dGrid.fill=GridBagConstraints.BOTH;

        panelClient.add(clientID,dGrid);
        dGrid.gridx=2;
        dGrid.gridy=1;
        dGrid.fill=GridBagConstraints.HORIZONTAL;

        panelClient.add(clientIDField,dGrid);
        dGrid.gridx=1;
        dGrid.gridy=2;
        dGrid.fill=GridBagConstraints.BOTH;
        panelClient.add(fname,dGrid);
        dGrid.gridx=2;
        dGrid.gridy=2;
        dGrid.fill=GridBagConstraints.HORIZONTAL;
        panelClient.add(fnameField,dGrid);
        dGrid.gridx=1;
        dGrid.gridy=3;
        panelClient.add(lname,dGrid);
        dGrid.gridx=2;
        dGrid.gridy=3;
        panelClient.add(lnameField,dGrid);
        dGrid.gridx=1;
        dGrid.gridy=4;
        panelClient.add(address,dGrid);
        dGrid.gridx=2;
        dGrid.gridy=4;
        panelClient.add(addressField,dGrid);
        dGrid.gridx=1;
        dGrid.gridy=5;
        panelClient.add(postalCode,dGrid);
        dGrid.gridx=2;
        dGrid.gridy=5;
        panelClient.add(postalCodeField,dGrid);
        dGrid.gridx=1;
        dGrid.gridy=6;
        panelClient.add(phone,dGrid);
        dGrid.gridx=2;
        dGrid.gridy=6;
        panelClient.add(phoneField,dGrid);
        dGrid.gridx=1;
        dGrid.gridy=7;
        panelClient.add(clientType,dGrid);
        dGrid.gridx=2;
        dGrid.gridy=7;
        panelClient.add(comboClient,dGrid);

        dGrid.gridx=1;
        dGrid.gridy=8;
        panelClient.add(save,dGrid);
        dGrid.gridx=2;
        dGrid.gridy=8;
        panelClient.add(delete,dGrid);
        dGrid.gridx=3;
        dGrid.gridy=8;
        panelClient.add(clear,dGrid);


        /**Tabbed Panel and panels**/
        tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Search Client",panelFinalSearch);
        tabbedPane.addTab("Enter Client Info",panelClient);

        /***Panels added to frame***/
        add(tabbedPane);

        /***Add actionListeners to components ***/
        save.addActionListener(listener);
        delete.addActionListener(listener);
        clear.addActionListener(listener);
        search.addActionListener(listener);
        clearSearch.addActionListener(listener);

        /***Add Document Listener to components ***/
       // searchField.,clientIDField,fnameField,lnameField,addressField,postalCodeField,phoneField
    }

/*    public static void main(String args[])
    {
        // Create the frame
        MyFrame frame = new MyFrame("Client Management Screen");
        frame.setSize(700,600);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }*/
    class JTextFieldLimit extends PlainDocument {
        private int limit;
        JTextFieldLimit(int limit) {
            super();
            this.limit = limit;
        }

        JTextFieldLimit(int limit, boolean upper) {
            super();
            this.limit = limit;
        }

        public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
            if (str == null)
                return;

            if ((getLength() + str.length()) <= limit) {
                super.insertString(offset, str, attr);
            }
        }
    }


}
