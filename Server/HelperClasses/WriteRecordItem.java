package Server.HelperClasses;

import Server.HelperClasses.ServerSock;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class WriteRecordItem {
    //private BufferedReader stdIn;
    private BufferedReader socketIn;
    private PrintWriter socketOut;
/*
    public BufferedReader getStdIn() {
        return stdIn;
    }

    public void setStdIn(BufferedReader stdIn) {
        this.stdIn = stdIn;
    }
*/
    public BufferedReader getSocketIn() {
        return socketIn;
    }

    public void setSocketIn(BufferedReader socketIn) {
        this.socketIn = socketIn;
    }

    public PrintWriter getSocketOut() {
        return socketOut;
    }

    public void setSocketOut(PrintWriter socketOut) {
        this.socketOut = socketOut;
    }

    public WriteRecordItem(ServerSock server){
        try{
            //stdIn = new BufferedReader(new InputStreamReader(System.in));
            socketIn = new BufferedReader(new InputStreamReader(server.getaSocket().getInputStream()));
            socketOut = new PrintWriter((server.getaSocket().getOutputStream()),true);
        }catch (IOException ex){
            System.err.println(ex.getStackTrace());
        }
    }


  /*  public void communicate()  {

        String line = "";
        String response = "";
        boolean running = true;
        while (running) {
            try {
                System.out.println("please enter a word: ");
                line = stdIn.readLine();
                if (!line.equals("QUIT")){
                    System.out.println(line);
                    socketOut.println(line);
                    response = socketIn.readLine();
                    System.out.println(response);
                }else{
                    running = false;
                }

            } catch (IOException e) {
                System.out.println("Sending error: " + e.getMessage());
            }
        }
        try {
            stdIn.close();
            socketIn.close();
            socketOut.close();
        } catch (IOException e) {
            System.out.println("Closing error: " + e.getMessage());
        }

    }

*/
}
