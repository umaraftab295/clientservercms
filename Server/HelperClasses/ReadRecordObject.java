package Server.HelperClasses;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import Model.Client;

public class ReadRecordObject {
    private ObjectInputStream objectIn = null;
    private Client record = null;
    private ArrayList<Client> clientArrayList;

    public ObjectInputStream getObjectIn() {
        return objectIn;
    }

    public void setObjectIn(ObjectInputStream objectIn) {
        this.objectIn = objectIn;
    }

    public ReadRecordObject(ServerSock server){
        clientArrayList = new ArrayList<Client>();
        try{
            objectIn = new ObjectInputStream(server.getaSocket().getInputStream());
        }catch (IOException ex){
            System.err.println(ex.getStackTrace());
        }
    }

    public Client openObjectInputStream(){
        try{
            record = (Client) objectIn.readObject();
        }catch(IOException ex){
            System.err.println("Error writing to a file");
        }catch(NoSuchElementException ex){
            System.err.println("Invalid Exception");
        }catch(ClassNotFoundException ex){
            System.err.println("Class not Found");
        }
        return record;
    }
    public ArrayList<Client> openObjectInputStreamArrayList(){

        try{
            while(true) {
                record = (Client) objectIn.readObject();
                clientArrayList.add(record);
            }
        }catch(IOException ex){
            System.err.println("Error writing to a file");
        }catch(NoSuchElementException ex){
            System.err.println("Invalid Exception");
        }catch(ClassNotFoundException ex){
            System.err.println("Class not Found");
        }
        return clientArrayList;
    }
}
