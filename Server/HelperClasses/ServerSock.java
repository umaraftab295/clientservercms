package Server.HelperClasses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSock {
    //PrintWriter out;
    private Socket aSocket;
    private ServerSocket serverSocket;
    //BufferedReader in;

    public Socket getaSocket() {
        return aSocket;
    }

    public void setaSocket(Socket aSocket) {
        this.aSocket = aSocket;
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    public void setServerSocket(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }

    public ServerSock(){
        try{
            serverSocket = new ServerSocket(8099);
        }catch(IOException ex){
            System.out.println("Create the new socket error");
        }
        System.out.println("Server is running");
    }



}
