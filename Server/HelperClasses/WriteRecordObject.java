package Server.HelperClasses;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.NoSuchElementException;
import java.util.ArrayList;
import Model.Client;


public class WriteRecordObject {
	private ArrayList<Client> clientArrayList;
	private ObjectOutputStream objectOut = null;
	private Client record = null;

	public WriteRecordObject(ServerSock server){
		clientArrayList = new ArrayList<Client>();
		try{
		objectOut = new ObjectOutputStream(server.getaSocket().getOutputStream());
		}catch (IOException ex){
			System.err.println(ex.getStackTrace());
		}
	}

	public Client openObjectOutputStream(Client client){
		try{
			record = client;
			objectOut.writeObject(record);
		}catch(IOException ex){
			System.err.println("Error writing to a file");
		}catch(NoSuchElementException ex){
			System.err.println("Invalid Exception");
		}
		return record;
	}

	public ArrayList<Client> openObjectArrayListOutputStream(ArrayList<Client> clientList){

		try{
			for(int i=0;i<clientList.size();i++){
				clientArrayList.add(clientList.get(i));
				objectOut.writeObject(clientList.get(i));

			}
		}catch(IOException ex){
			System.err.println("Error writing to a file");
		}catch(NoSuchElementException ex) {
			System.err.println("Invalid Exception");
		}
		return clientArrayList;
	}

}
