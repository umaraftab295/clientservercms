package Server.DTO;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;
import Model.Client;

public class ClientCMS {

        public Connection jdbc_connection;
        public PreparedStatement statement = null;
        public String databaseName = "clientmanagement", tableName = "Client", dataFile = "clients.txt";

        public String connectionInfo = "jdbc:mysql://localhost:3306/"+databaseName,
                login          = "umar",
                password       = "Umar@295";


        /**
         * Constructor which initializes the Driver Manager
         */
        public ClientCMS()
        {
            try{
                // If this throws an error, make sure you have added the mySQL connector JAR to the project
                Class.forName("com.mysql.jdbc.Driver");

                // If this fails make sure your connectionInfo and login/password are correct
                jdbc_connection = DriverManager.getConnection(connectionInfo, login, password);
                System.out.println("Connected to: " + connectionInfo + "\n");
            }
            catch(SQLException e) { e.printStackTrace(); }
            catch(Exception e) { e.printStackTrace(); }
        }

        /**
         * Creates DB if not exists
         */
        public void createDB()
        {
            try {
                String SQL= "CREATE DATABASE" + databaseName+"IF NOT EXISTS";
                statement = jdbc_connection.prepareStatement(SQL);
                statement.executeUpdate();
                System.out.println("Created Database " + databaseName);
            }
            catch( SQLException e)
            {
                e.printStackTrace();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        //

        /**
         * Create a data table inside of the database to hold Clients
         */
        public void createTable()
        {
            String sql = "CREATE TABLE " + tableName + "(" +
                    "int INT(4) NOT NULL, " +
                    "firstname VARCHAR(20) NOT NULL, " +
                    "lastname VARCHAR(20) NOT NULL, " +
                    "address VARCHAR(50) NOT NULL, " +
                    "postalCode CHAR(7) NOT NULL, " +
                    "phoneNumber CHAR(12) NOT NULL, " +
                    "clientType CHAR(1) NOT NULL, " +
                    "PRIMARY KEY ( id ))";
            try{
                statement = jdbc_connection.prepareStatement(sql);
                statement.executeUpdate();
                System.out.println("Created Table " + tableName);
            }
            catch(SQLException e)
            {
                e.printStackTrace();
            }
        }

        //

        /**
         * Removes the data table from the database (and all the data held within it!)
         */
        public void removeTable()
        {
            String sql = "DROP TABLE " + tableName;
            try{
                statement = jdbc_connection.prepareStatement(sql);
                statement.executeUpdate();
                System.out.println("Removed Table " + tableName);
            }
            catch(SQLException e)
            {
                e.printStackTrace();
            }
        }

        //

        /**
         * Fills the data table with all the clients from the text file 'items.txt' if found
         */
        public void fillTable()
        {
            try{
                Scanner sc = new Scanner(new FileReader(dataFile));
                while(sc.hasNext())
                {
                    String clientInfo[] = sc.nextLine().split(";");
                    addClient( new Client(
                            clientInfo[0].toString(),
                            clientInfo[1].toString(),
                            clientInfo[2].toString(),
                            clientInfo[3].toString(),
                            clientInfo[4].toString(),
                            clientInfo[5].toString()
                            )
                    );
                }
                sc.close();
            }
            catch(FileNotFoundException e)
            {
                System.err.println("File " + dataFile + " Not Found!");
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        //

        /**
         * Add a client to the database table
         * @param client
         */
        public void addClient(Client client)
        {
            String sql = "INSERT INTO " + tableName +" (FIRSTNAME,LASTNAME,ADDRESS,POSTALCODE,PHONENUMBER,CLIENTTYPE) "+"VALUES (?,?,?,?,?,?);";
            try{
                statement = jdbc_connection.prepareStatement(sql);
                statement.setString(1,client.getFirstname());
                statement.setString(2,client.getLastname());
                statement.setString(3,client.getAddress());
                statement.setString(4,client.getPostalCode());
                statement.setString(5,client.getPhoneNumber());
                statement.setString(6,client.getClientType());
                statement.executeUpdate();
            }
            catch(SQLException e)
            {
                e.printStackTrace();
            }
        }

        public void updateClient(Client client){
            String sql = "UPDATE "+tableName+" SET "+
                        "FIRSTNAME="+"?,"+
                        "LASTNAME="+"?,"+
                        "ADDRESS="+"?,"+
                        "POSTALCODE="+"?,"+
                        "PHONENUMBER="+"?,"+
                        "CLIENTTYPE="+"? "+
                        "WHERE ID=" + "?";
            try{
                statement = jdbc_connection.prepareStatement(sql);
                statement.setString(1,client.getFirstname());
                statement.setString(2,client.getLastname());
                statement.setString(3,client.getAddress());
                statement.setString(4,client.getPostalCode());
                statement.setString(5,client.getPhoneNumber());
                statement.setString(6,client.getClientType());
                statement.setInt(7,client.getId());
                statement.executeUpdate();
            }
            catch(SQLException e)
            {
                e.printStackTrace();
            }

        }

        public void deleteClient(int clientID){
            String sql = "DELETE FROM " + tableName + " WHERE ID=" + "?";
            try {
                statement = jdbc_connection.prepareStatement(sql);
                statement.setInt(1, clientID);
                statement.executeUpdate();
            }catch (SQLException e) {
                e.printStackTrace();

            }

        }


        /**
         * This method should search the database table for a client matching the clientID parameter and return that client.
         * @param clientID
         * @return
         */
        public Client searchClientByID(int clientID)
        {
            String sql = "SELECT * FROM "+tableName+" WHERE ID="+"?";
            ResultSet client;
            try {
                statement = jdbc_connection.prepareStatement(sql);
                statement.setInt(1,clientID);
                client = statement.executeQuery();
                System.out.println(client);
                if(client.next())
                {
                    return new Client(client.getInt("id"),
                            client.getString("firstname"),
                            client.getString("lastname"),
                            client.getString("address"),
                            client.getString("postalCode"),
                            client.getString("phoneNumber"),
                            client.getString("clientType")
                            );

                }

            } catch (SQLException e) { e.printStackTrace(); }

            return null;
        }

    /**
     * This method should search the database table for a client matching the clientID parameter and return that client.
     * @paramLastName
     * @return
     */
    public ArrayList<Client> searchClientByLastName(String lastname)
    {
        String sql = "SELECT * FROM " + tableName + " WHERE LASTNAME=" + "?";
        ResultSet client;
        ArrayList<Client> resultClients = new ArrayList<Client>();
        try {
            statement = jdbc_connection.prepareStatement(sql);
            statement.setString(1,lastname);
            client = statement.executeQuery();
            while(client.next())
            {
                resultClients.add(
                        new Client(client.getInt("ID"),
                        client.getString("FIRSTNAME"),
                        client.getString("LASTNAME"),
                        client.getString("ADDRESS"),
                        client.getString("POSTALCODE"),
                        client.getString("PHONENUMBER"),
                        client.getString("CLIENTTYPE"))
                );
            }
            return resultClients;

        } catch (SQLException e) { e.printStackTrace(); }

        return null;
    }



    /**
     * This method should search the database table for a client matching the clientID parameter and return that client.
     * @paramclientType
     * @return
     */
    public ArrayList<Client> searchClientByClientType(String clientType)
    {
        String sql = "SELECT * FROM "+tableName+" WHERE clientType="+"?";
        ResultSet client;
        ArrayList<Client> resultClients = new ArrayList<Client>();
        try {
            statement = jdbc_connection.prepareStatement(sql);
            statement.setString(1,clientType);
            client = statement.executeQuery();
            while(client.next())
            {
                resultClients.add(
                        new Client(client.getInt("ID"),
                        client.getString("FIRSTNAME"),
                        client.getString("LASTNAME"),
                        client.getString("ADDRESS"),
                        client.getString("POSTALCODE"),
                        client.getString("PHONENUMBER"),
                        client.getString("CLIENTTYPE"))
                );
            }
            return resultClients;

        } catch (SQLException e) { e.printStackTrace(); }

        return null;
    }

        /**
         * Prints all the items in the database to console
         */
        public void printTable()
        {
            try {
                String sql = "SELECT * FROM " + tableName;
                statement = jdbc_connection.prepareStatement(sql);
                ResultSet clients = statement.executeQuery();
                System.out.println("CLIENTS:");
                while(clients.next())
                {
                    System.out.println(clients.getInt("ID") + " " +
                            clients.getString("FIRSTNAME") + " " +
                            clients.getString("LASTNAME") + " " +
                            clients.getString("ADDRESS") + " " +
                            clients.getString("POSTALCODE") + " " +
                            clients.getString("PHONENUMBER") + " " +
                            clients.getString("CLIENTTYPE"));
                }
                clients.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
}
