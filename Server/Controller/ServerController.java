package Server.Controller;

import Server.HelperClasses.ServerSock;
import Server.HelperClasses.WriteRecordItem;
import Server.HelperClasses.ReadRecordObject;
import Server.HelperClasses.WriteRecordObject;
import Model.Client;
import Server.DTO.ClientCMS;

import java.io.IOException;
import java.util.ArrayList;

public class ServerController {
    private ServerSock server;
    private WriteRecordObject objectWrite;
    private WriteRecordItem itemWrite;
    private ReadRecordObject objectRead;
    private Client client;
    private ArrayList<Client> clientArrayList;
    private ClientCMS clientCMS;

    public ServerSock getServer() {
        return server;
    }

    public void setServer(ServerSock server) {
        this.server = server;
    }

    public WriteRecordObject getObjectWrite() {
        return objectWrite;
    }

    public void setObjectWrite(WriteRecordObject objectWrite) {
        this.objectWrite = objectWrite;
    }

    public WriteRecordItem getItemWrite() {
        return itemWrite;
    }

    public void setItemWrite(WriteRecordItem itemWrite) {
        this.itemWrite = itemWrite;
    }

    public ReadRecordObject getObjectRead() {
        return objectRead;
    }

    public void setObjectRead(ReadRecordObject objectRead) {
        this.objectRead = objectRead;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public ArrayList<Client> getClientArrayList() {
        return clientArrayList;
    }

    public void setClientArrayList(ArrayList<Client> clientArrayList) {
        this.clientArrayList = clientArrayList;
    }

    public ClientCMS getClientCMS() {
        return clientCMS;
    }

    public void setClientCMS(ClientCMS clientCMS) {
        this.clientCMS = clientCMS;
    }


    public ServerController(ServerSock server){
        this.server = server;
        itemWrite = new WriteRecordItem(server);
        objectRead = new ReadRecordObject(server);
        objectWrite = new WriteRecordObject(server);
        clientCMS = new ClientCMS();

    }

    public Client searchClientByID(int clientId) {
        client = clientCMS.searchClientByID(clientId);
        return objectWrite.openObjectOutputStream(client);
    }

    public ArrayList<Client> searchClientByLastName(String lName){
        clientArrayList = clientCMS.searchClientByLastName(lName);
        return objectWrite.openObjectArrayListOutputStream(clientArrayList);
    }

    public ArrayList<Client> searchClientByClientType(String clientType){
        clientArrayList = clientCMS.searchClientByClientType(clientType);
        return objectWrite.openObjectArrayListOutputStream(clientArrayList);
    }

    public void addClient(Client client){
        clientCMS.addClient(client);
    }

    public void updateClient(Client client){
        clientCMS.updateClient(client);
    }

    public void deleteClient(int clientId){
        clientCMS.deleteClient(clientId);
    }

}
