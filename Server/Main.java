package Server;

import Server.Controller.ServerController;
import Server.HelperClasses.ServerSock;
import Model.Client;
import java.io.IOException;

public class Main {
    public static void main(String[] args){
        Client client;
        ServerSock server = new ServerSock();

        try{
            server.setaSocket(server.getServerSocket().accept());
            System.out.println("Accepted Socket");
      /*
      serv.in = new BufferedReader(new InputStreamReader(serv.aSocket.getInputStream()));
      serv.out = new PrintWriter((serv.aSocket.getOutputStream()),true);
      serv.in.close();
      serv.out.close();
      */
        }catch (IOException ex){
            System.out.println(ex.getMessage());
        }
        ServerController controller = new ServerController(server);
        try {
            switch (controller.getItemWrite().getSocketIn().readLine()) {
                case "searchClientByID":
                    // code block
                    String id =controller.getItemWrite().getSocketIn().readLine();
                    System.out.println("The id is"+id);
                    int clientId= Integer.parseInt(id);
                    System.out.println("searchClientByID method is invoked");
                    controller.searchClientByID(clientId);
                    break;
                case "searchClientByLastName":
                    // code block
                    String lName= controller.getItemWrite().getSocketIn().readLine();
                    controller.searchClientByLastName(lName);
                    System.out.println("searchClientByLastName method is invoked");
                    break;
                case "searchClientByClientType":
                    String clientType=controller.getItemWrite().getSocketIn().readLine();
                    controller.searchClientByClientType(clientType);
                    System.out.println("searchClientByClientType method is invoked");
                    break;
                case "addClient":
                    client= controller.getObjectRead().openObjectInputStream();
                    controller.addClient(client);
                    System.out.println("addClient method is invoked");
                    break;
                case "updateClient":
                    client= controller.getObjectRead().openObjectInputStream();
                    controller.updateClient(client);
                    System.out.println("updateClient method is invoked");
                    break;
                case "deleteClient":
                    String idD =controller.getItemWrite().getSocketIn().readLine();
                    System.out.println("The id is"+idD);
                    int clientIdD= Integer.parseInt(idD);
                    System.out.println("deleteClient method is invoked");
                    controller.deleteClient(clientIdD);
                    break;
                default:
                    // code block
            }
        }catch(IOException ex){
            System.out.println(ex.getMessage());
        }

    }

}
